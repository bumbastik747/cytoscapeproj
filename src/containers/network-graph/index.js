import React, { Component } from 'react';
import NetworkGraph from '../../components/network-graph/';
import { toolbarItems as items} from '../../ToolbarItems';

export default class NetworkGraphContainer extends Component {
  state = {
    status: 'fetching',
    toolbarItems: null,
  };

  componentDidMount() {
    this.getToolbarItems();
  }

  getToolbarItems = () => {
    //just for example
    setTimeout(
      () =>
        this.setState({
          status: 'fetched',
          toolbarItems: items,
        }),
      1500
    );
  };

  render() {
    return (
      <NetworkGraph
        containerId="cytoscapeContainer"
        toolbarItems={this.state.toolbarItems}
      />
    );
  }
}
