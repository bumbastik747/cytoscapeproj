import React from 'react';
import './NetworkGraphSidebar.css';

export default class NetworkGraphSidebar extends React.Component {
  sidebarElement = null;
  overlayElement = null;

  componentDidMount() {
    if (this.props.opened) {    
      this.open();
    }
  }

  componentDidUpdate() {
    if (this.props.opened) {
      this.open();
    }
  }

  open() {
    const { openHandler } = this.props;
    if (this.sidebarElement && this.overlayElement) {
      this.sidebarElement.style.right = '0';
      this.overlayElement.style.visibility = 'visible';
    }
    openHandler && openHandler();
  }

  close = () => {
    if (this.sidebarElement && this.overlayElement) {
      this.sidebarElement.style.right = '-400px';
      setTimeout(() => this.overlayElement.style.visibility = 'hidden', 300);
    }
  };

  render() {
    const { children, height } = this.props;
    return (
      <div className="network-graph-sidebar-overlay" ref={elem => this.overlayElement = elem}>
        <aside className="network-graph-sidebar" style={{ height }} ref={elem => this.sidebarElement = elem}>
          <header className="network-graph-sidebar-header">
            <div className="network-graph-sidebar-header__closeBtn" onClick={this.close}>&times;</div>
            <div className="network-graph-sidebar-header__text">Header Text</div>
          </header>
          {typeof children === 'function' ? children() : children}
        </aside>
      </div>
    );
  }
}
