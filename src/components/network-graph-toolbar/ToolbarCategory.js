import React from 'react';
import ToolbarItem from './ToolbarItem';
import {Tooltip} from '../common/Tooltip';

const ToolbarCategory = ({ title, items, ...rest }) => (
  <div className="network-graph-toolbar-category">
    <p className="network-graph-toolbar-category__title">{title}</p>
    <div className="network-graph-toolbar-category-container">
      {items.map((item, index) => (
        <Tooltip content={item.title} arrow arrowType='sharp' placement='right' key={`toolbarItems${index}`}>
          <ToolbarItem
            {...item}
            {...rest}
            category={title}
          />
        </Tooltip>
      ))}
    </div>
  </div>
);

export default ToolbarCategory;