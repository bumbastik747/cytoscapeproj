import React from 'react';
import ToolbarCategory from './ToolbarCategory';
import './NetworkGraphToolbar.css';

class NetworkGraphToolbar extends React.Component {
  getItemsSortedByCategory() {
    const { items, ...rest } = this.props;
    const resultItems = [];
    for (let category in items) {
      resultItems.push(
        <ToolbarCategory
          title={category}
          items={items[category]}
          key={category}
          {...rest}
        />
      );
    }
    return resultItems;
  }
  render() {
    const toolbarItems = this.getItemsSortedByCategory();
    return <div className="network-graph-toolbar">{toolbarItems}</div>;
  }
}

export default NetworkGraphToolbar;
