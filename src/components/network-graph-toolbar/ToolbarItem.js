import React from 'react';

class ToolbarItem extends React.Component {
  clickHandler = () => {
    const { itemClickHandler, type, category, ...rest } = this.props;
    itemClickHandler({ type, category, ...rest});
  }

  dragStartHandler = e => {
    const { type, category, ...rest } = this.props;
    e.dataTransfer.setData('options', JSON.stringify({ type, category, ...rest}));
  };

  render() {
    const { title, icon, type } = this.props;
    return (
      <div
        className="network-graph-toolbar-item"
        draggable
        onClick={this.clickHandler}
        onDragStart={this.dragStartHandler}
        data-toolbar-item-type={type}
      >
        <div className="network-graph-toolbar-item__container-icon">
          <img src={icon} width="28px" height="28px" />
        </div>
        <div className="network-graph-toolbar-item__container-title">
          {title}
        </div>
      </div>
    );
  }
}

export default ToolbarItem;