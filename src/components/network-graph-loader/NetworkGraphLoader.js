import React from 'react';
import './index.css';
import { GridLoader } from 'react-spinners';

export const NetworkGraphLoader = () => {
  return (
    <div className='network-graph-loader-container'>
      <GridLoader color="orange" className="" loading size={50} />
    </div>
  );
};
