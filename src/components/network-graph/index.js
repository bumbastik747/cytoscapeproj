import React, { Component } from 'react';
import NetworkGraphToolbar from '../network-graph-toolbar';
import CytoGraph from '../../CytoGraph/';
import { NetworkGraphLoader } from '../network-graph-loader/NetworkGraphLoader';
import NetworkGraphSidebar from '../network-graph-sidebar/';
import './NetworkGraph.css';

class NetworkGraph extends Component {
  state = {
    sidebarOpened: false,
  };

  graph = null;
  graphContainer = null;

  static defaultProps = {
    graphOptions: null,
    containerId: 'sampleContainerId',
    graphWidth: '100%',
    graphHeight: '1000px',
  };

  addNode = options => {
    this.graph.addNode(options);
  };

  openSideBar = () => this.setState({ sidebarOpened: true });

  componentDidUpdate() {
    if (!this.graph) {
      this.createGraph();
    }
  }

  componentDidMount() {
    this.createGraph();
  }

  createGraph() {
    if (!this.graph && this.props.toolbarItems) {
      const { graphOptions } = this.props;
      const defaultOptions = { container: this.graphContainer };
      this.graph = new CytoGraph({
        graphOptions: graphOptions ? graphOptions : defaultOptions,
      });
      this.graph.nodesOn('doubleTap', this.openSideBar);
    }
  }

  cancelEvent(e) {
    e.persist();
    e.preventDefault();
  }

  dragAddNodeHandler = e => {
    if (e && e.target) {
      e.preventDefault();
      const container = e.target.parentElement;
      if (container && container.id === this.props.containerId) {
        const { clientX, clientY } = e;
        const optionsStr = e.dataTransfer.getData('options');
        const options = {
          ...JSON.parse(optionsStr),
          dropPosition: { x: clientX, y: clientY },
        };
        this.addNode(options);
      }
    }
    return false;
  };

  render() {
    const {
      containerId = 'sampleContainerId',
      toolbarItems,
      graphWidth = '100%',
      graphHeight = '1000px',
    } = this.props;
    const { sidebarOpened } = this.state;

    if (!toolbarItems) {
      return <NetworkGraphLoader />;
    }
    return (
      <React.Fragment>
        <div
          className="network-graph"
          style={{ width: graphWidth, height: graphHeight }}
        >
          <NetworkGraphToolbar
            items={toolbarItems}
            itemClickHandler={this.addNode}
          />
          <div
            id={containerId}
            className="network-graph-canvas-container"
            ref={node => (this.graphContainer = node)}
            onDragOver={this.cancelEvent}
            onDragEnd={this.cancelEvent}
            onDrop={this.dragAddNodeHandler}
          />
        </div>
        <NetworkGraphSidebar height={graphHeight} opened={sidebarOpened}>
          <div>content</div>
        </NetworkGraphSidebar>
      </React.Fragment>
    );
  }
}

export default NetworkGraph;
