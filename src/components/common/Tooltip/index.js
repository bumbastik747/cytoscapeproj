import React from 'react';
import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';

export const Tooltip = ({ children, ...tippyProps }) => (
  <Tippy {...tippyProps}>
    {typeof children === 'function' ? children() : children}
  </Tippy>
);
