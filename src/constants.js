export const nodeTypes = {
  FILE: 'FILE',
  DATASETS: 'DATASETS',
  TREE_VIEWER: 'TREE_VIEWER',
  SCATTER_PLOT: 'SCATTER_PLOT',
  TREE: 'TREE',
  SVM: 'SVM',

};

export const defaultNodeOptions = {
  position: {
    x: 300,
    y: 300
  }
};

export const nodeOptions = {
  [nodeTypes.FILE]: {
  }
};

