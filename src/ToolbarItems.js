import File from './public/icons/File.svg';
import DataSets from './public/icons/DataSets.svg';
import SQLTable from './public/icons/SQLTable.svg';
import Table from './public/icons/Table.svg';
import PCA from './public/icons/PCA.svg';
import Transpose from './public/icons/Transpose.svg';

import { nodeTypes } from "./constants.js";

const { FILE, DATASETS, TREE_VIEWER, SCATTER_PLOT, TREE, SVM } = nodeTypes;

export const toolbarItems = {
  Data: [
    {
      title: "File",
      type: FILE,
      icon: File,
      color: 'orange'
    },
    {
      title: "Datasets",
      type: DATASETS,
      icon: DataSets,
      color: 'orange'
    }
  ],
  Visualize: [
    {
      title: "Tree viewer",
      type: TREE_VIEWER,
      icon: SQLTable,
      color: 'lightcoral'
    },
    {
      title: "Scatter plot",
      type: SCATTER_PLOT,
      icon: Table,
      color: 'lightcoral'
    }
  ],
  Model: [
    {
      title: "SVM",
      type: SVM,
      icon: PCA,
      color: 'lightpink'
    },
    {
      title: "Tree",
      type: TREE,
      icon: Transpose,
      color: 'lightpink'
    }, 
  ]
};
