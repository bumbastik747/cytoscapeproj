import FileIcon from '../../public/icons/File.svg';
import { nodeTypes } from '../../constants';

const { FILE } = nodeTypes;

const warn = (msg = '') => true && console.warn(msg);

const defaultContainer = document.body.appendChild(
  document.createElement('div')
);

export const buildGraphOptions = ({
  container = warn(
    'You should provide a container node! Default container is just a mock!'
  ) && defaultContainer,
  elements = [],
  options = null,
}) =>
  options
    ? options
    : {
        //common Options
        container: container,
        elements: elements,
        ...graphInitialOptions,
      };

const defaultGraphStyles = [
  // the stylesheet for the graph
  {
    selector: 'node',
    style: {
      width: '120px',
      height: '40px',
      label: 'data(title)',
      shape: 'roundrectangle',
    },
  },
  {
    selector: 'node:selected',
    style: { 
      'background-color': 'green',
    },
  },
  {
    selector: 'edge',
    style: {
      width: 3,
      'line-color': '#ccc',
      'target-arrow-color': '#ccc',
      'target-arrow-shape': 'triangle',
      'curve-style': 'unbundled-bezier',
      'control-point-distances': '20 -20',
      'control-point-weights': '0.25 0.75',
    },
  },
  {
    selector: '.eh-handle',
    style: {
      width: '10px',
      height: '10px',
      'background-color': 'black',
      padding: '0 0 0 10px',
    },
  },
  // {
  //   selector: `[type="${FILE}"]`,
  //   style: {
  //     'background-image': FileIcon,
  //     'background-color': 'orange',
  //   },
  // },
  // {
  //   selector: `[type="${FILE}"]:selected`,
  //   style: {
  //     'background-image': FileIcon,
  //     'background-color': 'green',
  //   },
  // },
];

const graphInitialOptions = {
  style: defaultGraphStyles,
  // initial viewport state:
  zoom: 1,
  pan: { x: 0, y: 0 },

  // interaction options:
  minZoom: 0.5,
  maxZoom: 2,
  zoomingEnabled: true,
  userZoomingEnabled: true,
  panningEnabled: true,
  userPanningEnabled: true,
  boxSelectionEnabled: false,
  selectionType: 'single',
  touchTapThreshold: 8,
  desktopTapThreshold: 4,
  autolock: false,
  autoungrabify: false,
  autounselectify: false,

  // rendering options:
  headless: false,
  styleEnabled: true,
  hideEdgesOnViewport: false,
  hideLabelsOnViewport: false,
  textureOnViewport: false,
  motionBlur: false,
  motionBlurOpacity: 0.2,
  wheelSensitivity: 1,
  pixelRatio: 'auto',
};
