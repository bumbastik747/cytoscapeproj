import $ from 'jquery';
import cytoscape from 'cytoscape';
import edgehandles from 'cytoscape-edgehandles';
// import cytoscape from '../../../forks/cytoscape.js/build/cytoscape';
// import edgehandles from '../../../forks/cytoscape.js-edgehandles/cytoscape-edgehandles';
import contextMenus from 'cytoscape-context-menus';
import gridGuide from 'cytoscape-grid-guide';
import { nodeOptions } from '../constants.js';
import {
  buildGraphOptions,
  edgehandlesOptions,
  getContextMenuOptions,
  gridGuideOptions,
} from './configuration';
//Add custom styles

//Register extensions
cytoscape.use(edgehandles);
contextMenus(cytoscape, $);
gridGuide(cytoscape, $);

class CytoGraph {
  _container = null;
  nodeId = 0;
  lastNode = null;
  lastNodePosition = { x: 300, y: 300 };

  constructor({ graphOptions = {}, instanceHandlers = [] }) {
    const options = buildGraphOptions(graphOptions);
    const cxtMenuOptions = getContextMenuOptions(this.cxtMenuBindings);
    this.init({ cyOptions: options });
    this.registerExtensions({ cxtMenuOptions });
  }

  init({ cyOptions }) {
    this.cy = new cytoscape(cyOptions);
    console.log(this.cy);
    this._addDoubleTapEvent();
    this._container = this.cy.container();
  }

  registerExtensions({ cxtMenuOptions }) {
    this.eh = this.cy.edgehandles(edgehandlesOptions);
    this.contextMenu = this.cy.contextMenus(cxtMenuOptions);
    this.gridGuide = this.cy.gridGuide(gridGuideOptions);
  }

  getNodes() {
    return this.cy.nodes();
  }

  addNode = options => {
    const nodeOptions = this._getNodeOptions(options);
    const node = (this.lastNode = this.cy.add(nodeOptions));
    this._addNewNodeStyles(nodeOptions.data);
    this._bindDefaultNodeEventHandlers(node);
  };

  _newNodePositionHandler = e => {
    if (e.target.id() === this.lastNode.id()) {
      this.lastNodePosition = { ...e.target.position() };
      this.lastNodePosition.x += 150;
    }
  };

  _addDoubleTapEvent() {
    let firstTap = null;
    let tapTimeout = null;
    this.cy.on('tap', e => {
      let tapNow = e.target;
      if (tapTimeout && firstTap) {
        clearTimeout(tapTimeout);
      }
      if (firstTap === tapNow) {
        tapNow.trigger('doubleTap');
        firstTap = null;
      } else {
        tapTimeout = setTimeout(() => (firstTap = null), 300);
        firstTap = tapNow;
      }
    });
  }

  removeHandler = e => {
    this.cy.remove(e.target);
  };

  _bindDefaultNodeEventHandlers(node) {
    this.newNodeDefaultBindings.forEach(item => this.nodeOn({ node, ...item }));
  }

  nodeOn = ({ node, event, handler }) => node.on(event, handler);

  nodesOn = (event, handler) => this.cy.on(event, 'node', handler);

  _addNewNodeStyles({ icon = 'icon', color = 'orange', type }) {
    this.cy
      .style()
      .fromJson([
        ...this.cy.style().json(),
        {
          selector: `[type="${type}"]`,
          style: {
            'background-color': color,
            'background-image': icon,
          },
        },
        {
          selector: `[type="${type}"]:selected`,
          style: {
            'background-color': 'lightsteelblue',
            'background-image': icon,
          },
        },
      ])
      .update();
  }

  _calculateNewNodePosition(dropPosition) {
    if (dropPosition) {
      const { offsetLeft, offsetTop } = this._container;
      const { x, y } = dropPosition;
      this.lastNodePosition = { x: x - offsetLeft, y: y - offsetTop };
      // this.lastNodePosition = { x, y };
    }
    return { ...this.lastNodePosition };
  }

  _getNodeOptions({ type, category, dropPosition = null, icon, color }) {
    let commonOptions = {
      group: 'nodes',
      data: {
        id: this.nodeId,
        title: `${type}${this.nodeId}`,
        type,
        category,
        icon,
        color,
      },
      renderedPosition: this._calculateNewNodePosition(dropPosition),
      ...nodeOptions[type],
    };
    this.nodeId++;
    this.lastNodePosition.x += 150;
    return { ...nodeOptions[type], ...commonOptions };
  }

  cxtMenuBindings = [
    {
      id: 'remove',
      handler: this.removeHandler,
    },
  ];

  newNodeDefaultBindings = [
    {
      event: 'tap',
      handler: e => {},
    },
    { event: 'position', handler: this._newNodePositionHandler },
  ];
}
export default CytoGraph;
