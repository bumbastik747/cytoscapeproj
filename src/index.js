import React from 'react';
import ReactDOM from 'react-dom';
import NetworkGraphContainer from './containers/network-graph/';
import './index.css';
ReactDOM.render(<NetworkGraphContainer />, document.getElementById('root'));

